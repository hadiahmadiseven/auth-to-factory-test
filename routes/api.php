<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/',function (){
//    auth()->logout();
//    auth()->loginUsingId(14);
//    return auth()->user();
});
Route::prefix('v1/phone')->namespace('\App\Http\Controllers\Auth')->middleware('guest')->group(function (){
    Route::get('/login','AuthLoginController@showLogin')->name('auth.phone.login');
    Route::post('/login','AuthLoginController@login');
    Route::get('/register','AuthRegisterController@showRegister')->name('auth.phone.register');
    Route::post('/register','AuthRegisterController@register');
    Route::get('/token','TokenController@getToken')->name('auth.phone.token');
    Route::post('/token','TokenController@token');
});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
