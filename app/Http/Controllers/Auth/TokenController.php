<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ActiveCode;
use App\Models\User;
use http\Env\Response;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TokenController extends Controller
{
    public function getToken(Request $request)
    {
//        return response()->json($request->session()->has('auth'));
//        if (! $request->session()->has('auth')) {
//            return response()->json([
//                'massage'=>'اطلاعات کامل دریافت نشد',
//                'status'=>'403',
//            ]);
//        }
    }

    public function token(Request $request)
    {
        $data=$this->validate($request,[
            'token'=>'required|numeric',
            'user'=>'required|exists:users,id',
            'remember'=>'required',
        ]);
        $user=User::findOrFail($data['user']);
        $status=ActiveCode::verifyCode($data['token'],$user);

        if (!$status) {
            return $this->shildRequest($user);
        }
        if (auth()->LoginUsingId($data['user'],$data['remember'])) {
            $user->activeCode()->delete();
            return response()->json([
                'massage'=>'رمز وارد شده درست می باشد',
                'url'=>\url('/'),
            ]);
        }

        return response()->json($status);

    }

    /**
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function shildRequest($user): \Illuminate\Http\JsonResponse
    {
        $test = $user->activeCode()->first();
        if (empty($test)) {
            return response()->json([
                'massage'=>'کاربر یافت نشد'
            ]);
        }
        $test['repeat'] += 1;
        $user = $user->activeCode()->first()->update(['repeat' => $test['repeat']]);
        if ($test['repeat'] > 3) {
            return response()->json([
                'massage' => 'تعداد درخواست کد برای این شماره بیشتر از حد مجاز است',
                'url' => route('auth.phone.register')
            ]);
        }
        return response()->json([
            'massage' => 'کد وارد شده اشتباه است',
            'url' => route('auth.phone.login')
        ]);
    }
}
