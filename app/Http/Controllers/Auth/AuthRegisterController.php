<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class AuthRegisterController extends Controller
{
    public function showRegister()
    {
        return "showRegister";
    }

    public function register(Request $request)
    {
        $data=$this->validate($request,[
            'name'=>'required|min:3',
            'phone'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric|unique:users,phone',
        ]);
        $user=User::create($data);
        return response()->json([
            'massage'=>'نام شما ثبت شد',
            'user'=>$user->id,
            'url'=>\route('auth.phone.login')
        ]);
    }
}
