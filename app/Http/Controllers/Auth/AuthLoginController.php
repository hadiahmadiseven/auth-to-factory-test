<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ActiveCode;
use App\Models\User;
use App\Notifications\ActiveCode as ActiveCodeNotification;
use http\Env\Response;
use Illuminate\Http\Request;

class AuthLoginController extends Controller
{
    public function showLogin()
    {
        return 'test';
    }

    public function login(Request $request)
    {
        $data=$this->validate($request,[
            'phone'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric|exists:users,phone',
        ]);

        $user=User::wherePhone($data['phone'])->first();
        //create code
        $code=ActiveCode::gnarateCode($user);
        $test=$user->activeCode()->first();
        if ($test['repeat_sms'] >= 3) {
            return response()->json([
                'massage'=>'تعداد درخواست کد برای این شماره بیشتر از حد مجاز است',
                'url'=>route('auth.phone.register')
            ]);

        }

        //send sms
        $user->notify(new ActiveCodeNotification($code , $user->phone));
        //send sms and +repeat_sms
        $test['repeat_sms']+=1;
        $user->activeCode()->first()->update(['repeat_sms'=>$test['repeat_sms']]);

        //response massage
        return response()->json([
        'massage'=>'پیامک برای شما ارسال شد',
        'user'=>$user->id,
        'remember'=>$request->has('remember'),
    ]);

    }


}
