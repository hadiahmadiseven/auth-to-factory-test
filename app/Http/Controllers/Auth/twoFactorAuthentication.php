<?php

namespace App\Http\Controllers\Auth;

use App\Models\ActiveCode;
use App\Notifications\ActiveCode as ActiveCodeNotification;
use App\Notifications\LoginToWebsite as LoginToWebsiteNotification ;
use Illuminate\Http\Request;

trait twoFactorAuthentication
{
    public function loggedin(Request $request, $user)
    {
            auth()->logout();

            $request->session()->flash('auth',[
                'user_id'=>$user->id,
                'remember'=>$request->has('remember'),
                'using_sms'=>true,
            ]);

                $code=ActiveCode::gnarateCode($user);
                //TODO send sms
                //send email
//                $user->notify(new LoginToWebsiteNotification($code));
                //send sms
                $user->notify(new ActiveCodeNotification($code , $user->phone));

                $request->session()->push('auth.using_sms',true);

            return response()->json([
                'url'=>route('auth.phone.login')
            ]);

    }

}
