<?php

namespace App\Notifications;

use App\Notifications\channels\GasedakChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ActiveCode extends Notification
{
    use Queueable;

    public $code;
    public $phone;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($code , $phone)
    {
        $this->code=$code;
        $this->phone=$phone;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        return [GasedakChannel::class];
    }

    public function toGhasedakSms($notifiable)
    {
        return[
            'text'=>"کد فعال سازی حساب کاربری {$notifiable->name}:  \n {$this->code}",
            'number'=>$this->phone
        ];
    }

}
