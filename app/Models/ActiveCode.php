<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveCode extends Model
{
    protected $table='active_code';
    use HasFactory;
    public $timestamps=false;
    protected $fillable=['name','phone','code','repeat','repeat_sms'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function scopeVerifyCode($query , $code , $user)
    {
        return !! $user->activeCode()->whereCode($code)->where('expired_at','>',now())->where('repeat','<',3)->first();
    }

    public function scopeGnarateCode($query ,$user)
    {
        if ($code=$this->getAliveCodeForUser($user)) {
            $code=$code->code;
        }else{
            do{
                $code=mt_rand(100000,999999);
            }while($this->checkCodeIsUnique($user,$code));
            $user->activeCode()->create([
                'repeat'=>0,
                'repeat_sms'=>0,
                'code'=>$code,
                'expired_at'=>now()->addDays(1),
            ]);

        }
        return $code;
    }

    private function getAliveCodeForUser($user)
    {
        return $user->activeCode()->where('expired_at','>',now())->first();
    }

    private function checkCodeIsUnique($user, int $code)
    {
        return !! $user->activeCode()->whereCode($code)->first();
    }
}
